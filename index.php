<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Berlatih OOP PHP</h1>
    <h3>Release 0</h3>
    <?php require_once('animal.php');
        $sheep = new Animal("shaun");

        $sheep->getInfo();

        // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())   
    ?>

    <h3>Release 1</h3>
    <?php 
        require_once('frog.php');
        // index.php
        $kodok = new Frog("buduk");
        $kodok->getInfo();
        $kodok->jump() ; // "hop hop"
    ?>   
    <br>
    <?php
        require_once('ape.php');
        $sungokong = new Ape("kera sakti");
        $sungokong->getInfo();
        $sungokong->yell(); // "Auooo"
    ?>  
</body>
</html>