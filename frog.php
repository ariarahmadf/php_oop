<?php
require_once('animal.php');

class Frog extends Animal  {
    public function __construct($name, $legs = 4 ,$cold_blooded = 'no'){      
        parent::__construct($name, $legs, $cold_blooded);

    }

    public function getInfo() {
        parent::getInfo();
    }

    public function jump() {
        echo "Jump : Hoop Hoop" . "<br>";
    }
}
?>