<?php
require_once('animal.php');

class Ape extends Animal  {
    public $legs;
    public function __construct($name, $cold_blooded = 'no'){      
        parent::__construct($name, $cold_blooded);

        $this->legs = 2;
    }

    public function getInfo() {
        parent::getInfo();
    }

    public function yell() {
        echo "Yell : Auooo" . "<br>";
    }
}
?>